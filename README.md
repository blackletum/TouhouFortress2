![logo](https://i.imgur.com/zJCR1Nu.png)

![logo](https://i.imgur.com/m06oRwx.png)

Video here:
https://www.youtube.com/watch?v=UWzxuJl49GI

# Touhou Fortress 2
## omg it real



If you want to no longer see the intro videos, just add “-novid” to your launch options.


# Q&A

- So… what is this?
  - **Touhou Fortress 2** is a sourcemod where you play in a team-oriented FPS as cute anime girls.

- Is there anything I should do immediately after installing the mod?
  - Yes, *please* make sure to read this page in full and to follow the instructions contained in the “OPEN THIS FIRST” folder contained within this repository. 
  - Also, please make sure you add this to your launch options or certain maps will crash 100% of the time
    -*+r_hunkalloclightmaps 0*

- Does this have any requirements?
  - Yes, this mod requires Source SDK 2013 MP and Team Fortress 2 to be installed. 

- Can I install this on Windows or Linux?
  - Yup! The game is compiled for both!
 
- Why did you make it?
  - *I wish I knew.* It was originally going to be a quickly slapped together shitpost but then I started spending far too much time on it.
 
- Why does your team image up top look poopy?
  - Becuase I used mspaint, shut up
 
- What’s the deal with the viewmodels vs worldmodels?
  - The game this is built on uses v_models and w_models instead of the typical c_models that Team Fortress 2 has. It would take quite a bit of work to get them all working properly in first person, and I do not have the skills nor time to do so unfortunately.
 
- I’m shooting the enemy in the head but I’m not getting headshots. *Why?*
  - *Yeah uhh, good luck with Sniper.* The hitboxes are not edited to fit the new models. Again, not in my wheelhouse.

- I don't see any servers...
  - Definitely a possibility. Feel free to host one!

- What's the future hold for Touhou Fortress 2?
  - Absolutely *nothing!* (Probably)
 
- Does that intro video really contain the entirety of Bad Apple?
  - Yes.
 
- Wait, we have grenades?
  - *Yes.* Use F and G to throw them. H is your taunt button.

- Explain the armor system to me.
  - *Nah* - Sniper TF2

- I renamed the folder in sourcemods and am having issues now.
  - *Don't do that.*

- I have *totally legitimate mod that isn't just balloon fempyro* that I want to add to the game, can I ?!?!
  - Yeah nah... but if it's a TRUE Quality-of-Life improvement, you can make a pull request for it. That would be very much reviewed on a case-by-case basis.
 
- Can we contact you if we run into issues?
  - How dare you even *think* of speaking to me.
  - (Feel free to post in Issues)

# Installation

- You can go about this in a number of ways. One way is to use Github Desktop to clone the repository, another way is to simply click on the green “Code” button in the upper right of this page, and then click on “Download ZIP”. Just make sure that within your sourcemods folder, this folder is named “touhoufortress2” or you will run into many issues, including not being able to see servers.

# Special Thanks

“PF2 0.6 Team” - For making this code open sourced originally

“Loyalists” [gamebanana] - Without all his models this would've never come together

“Mr. Kit” & “LiamTouhou” - [gamebanana] Touhou Fortress 2 Reupload. Kit and his team put together the original ThF files and Liam preserved it on Gamebanana.

“hth57” [moddb] - Touhou Fortress 2 1.0007 English Version pack

“Mugi cool” [YouTube] - Bad Apple 4k

“Rina-chan” Utsuho-Soldier Voicepack [Touhou 2010]

“Midian-P” [deviantart] - Full name logo 

“Pants” [shrinemaiden.org] - Partial Icon

“Aregularhuman” [gamebanana] - Gohei weapon (world) model

Anyone else unfortunately not credited that I was not aware of or forgot!
